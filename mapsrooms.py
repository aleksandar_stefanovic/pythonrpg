#trap, health bonus, fight
import random
def descriptions(room, LevelOneRooms): #input room, outputs the description
    #Level 1
    if room == 111:
        print "START ROOM  Your teammates have shrunk you down to microscopic size. You must look around for a power source so that you can grow back to normal size."
        return 0,0
    
    elif room == int(str(LevelOneRooms[0:1]).replace("[",'').replace("]",'')):
        print "Upon making it to the beach you replenish yourself! but contract 2 E.coli FIGHT!"
        return "ecoli",2
    
    elif room == int(str(LevelOneRooms[1:2]).replace("[",'').replace("]",'')):
        print "What a pleasant smell, the mountainside is treating you well! Grab some nutrients and be on your way!"
        return 0,0
    
    elif room == int(str(LevelOneRooms[2:3]).replace("[",'').replace("]",'')):
        print "You reach a dry dessertlike area. There is an E. coli attacking you! FIGHT!"
        return "ecoli",1
    
    elif room == int(str(LevelOneRooms[3:4]).replace("[",'').replace("]",'')):
        print "Enjoying the crashing waves and sand, you walk into a drop of blood! NO! HIV are attacking! FIGHT!"
        return "hiv",3

    elif room == int(str(LevelOneRooms[4:5]).replace("[",'').replace("]",'')):
        print "You walk into a drop of mist and get carried upward, hitting the ceiling of the laboratory. You fall and lose half your HP!"
        return 0,0

    elif room == int(str(LevelOneRooms[5:6]).replace("[",'').replace("]",'')):
        print "You reach some sort of heat pocket. Two HIV viruses are attacking you! FIGHT!"
        return "hiv",2

    elif room == int(str(LevelOneRooms[6:7]).replace("[",'').replace("]",'')):
        print "Made it to the clearing! Continue on little warrior! You find a tiny stream but there are E.coli guarding a potion ! FIGHT!"
        return "ecoli",2

    elif room == int(str(LevelOneRooms[7:8]).replace("[",'').replace("]",'')):
        print "You enter a crack in a beaker but it is too dark to see! Something about this is off-putting. HIV are amassing. FIGHT! (Accuracy reduced by 10%)"
        return "hiv",3

    elif room == int(str(LevelOneRooms[8:9]).replace("[",'').replace("]",'')):
        print "Brrrr! It is cold here. Choose between <frostbite> and some <water> or safe passage! Hurry make a choice! OPTION!"
        return 0,0

    elif room == int(str(LevelOneRooms[9:10]).replace("[",'').replace("]",'')):
        print "You somehow get sucked into the labs vacuum chamber. Holding onto the brim of the vacuum, you just barely manage to escape!"
        return 0,0

    elif room == 143:
        print "BOSS ROOM  There is a single capacitor a power source! To bad that it is guarded by a giant amoeba. Take it down!"
        return "amoeba",1


    #Level 2
    elif room == 211:
        print "You walk into the dessert tank of the lab. Two ants want to grab you to eat. FIGHT EM OFF!!!"
        return "ant",2

    elif room == 212:
        print "START ROOM You have successfully grown to the size of a bug. You are not done yet; look for more power to grow back to normal size!"
        return 0,0

    elif room == 213:
        print "You climb up onto a random pebble you find. An earthworm was sunbathing and died from the heat. Gain 20% more HP from the meat."
        return 0,0

    elif room == 221:
        print "You walk under a strong lamp. GET OUTTA HERE!!! (for every second here, you lose 1 HP)"
        return 0,0

    elif room == 222:
        print "You walk onto a small puddle. Earthworms want that moist puddle for themselves. FIGHT!"
        return "worm",2

    elif room == 223:
        print "You walk under the skylight and get invigorated. An ant notices you. FIGHT!!"
        return "ant",1

    elif room == 231:
        print "A strong breeze from the fridge hits you. Some earthworms are dropped near you. They slither toward you threateningly. FIGHT! Get em away, GET EM AWAY!!!"
        return "worm",2

    elif room == 232:
        print "You reach the labs bonsai tree. Whos idea was it to get one anyway? You knock on the tree. An earthworm falls out onto you as another crawls out of the dirt. FIGHT!"
        return "worm",1

    elif room == 233:
        print "Walking into a beaker You lose your sense of direction. Fight the earth worm!"
        return "worm",1

    elif room == 241:
        print "A soothing breeze passes by as the outside door opens. Then, an ant gets flung into your face! It looks ticked off FIGHT! "
        return "ant",1

    elif room == 242:
        print "You somehow get sucked into the labs vacuum chamber. Holding onto the brim of the vacuum, you barely manage to escape!"
        return 0,0

    elif room == 243:
        print "BOSS ROOM  You find a D-battery a power source! Too bad that giant praying mantis wants it too FIGHT!"
        return "mantus",1


    #Level 3
    elif room == 311:
        print "R2D2 and C-3PO here! Need a message delivered, were your go to droids! Not android! Real cool droids! "
        return 0,0

    elif room == 312:
        print "Dreaming of Jedi and Sith cooperation, your awoken by sounds of war! Hurry, start running!"
        return 0,0

    elif room == 313:
        print "START ROOM  Finally! Back to normal. Your exlab-mates are still here, though. Fight through them to find who the heck is behind all of this. Also, they have done a makeover to the lab. Everything is Star Wars themed"
        return 0,0

    elif room == 321:
        soldier = raw_input("Rebels or Imperials!?!?!? Your fate descends from the response of this simple yet life altering question. Answer now! Rawrrrrr!")
        while "rebel" not in soldier and "imperial" not in soldier:
            soldier = raw_input("Rebels or Imperials!?!?!? Your fate descends from the response of this simple yet life altering question. Answer now! Rawrrrrr!")
        if "rebel" in soldier: soldier = "Imperial"
        else: soldier = "Rebel Alliance"
        print "You meet a " + soldier + "soldier! FIGHT! "
        return "traitor",1

    elif room == 322:
        print "You are onboard the Star Destroyer! Lucky you! -_-"      
        return 0,0

    elif room == 323:
        print "Welcome to the death star. Given death star and kills one monsters in either 3 levels."
        return 0,0

    elif room == 331:
        print "You witness the death of Chewbacca, trying to save Anakin Solo! Being so shaken up, you lose the ability to yield a critical attack!"
        return "traitor",1

    elif room == 332:
        print "You meet Anakin Skywalker, who seems to be nice! He poisons you! You lose 1/2 of your health!"
        return "traitor",1

    elif room == 333:
        print "At the death star, your granted the FORCE! Your critical damage is dealt next hit! "
        return 0,0

    elif room == 341:
        print "Meeting Hans solo and Princess Leia, you mention how cute they would be as a couple! They dont take it lightly, FIGHT! "
        return "traitor",2

    elif room == 342:
        print "Heading to control centre, you find a shield! You shall avoid damage for one fight!"
        return 0,0

    elif room == 343:
        print "FINAL ROOM   The guy behind the all of this! He is hooded. My god, he has gone mad with power! Take him down and find out who he is!!"
        return "theguy",1
    

def movement(begin): #begin (string) is the room your in, begin (str) is now the new room.
    temp = int(begin) #ex. 241
    begin = int(begin) % 100 #ex. 41
    hundreds = temp - begin #ex. 200
    begin = str(begin)
    allowedmove = []
    #print begin, ""
    if begin.endswith('1') is not True:
        allowedmove.append("north")
    if begin[:1].startswith('4') is not True:
        allowedmove.append("east")
    if begin.endswith('3') is not True:
        allowedmove.append("south")
    if begin[:1].startswith('1') is not True:
        allowedmove.append("west")
    print "You can move: ", allowedmove
    begin = int(begin)
    movewhere = raw_input("Where do you want to go: ")
    while movewhere not in allowedmove:
        movewhere = raw_input("Where do you want to go: ")
    if movewhere == "south":
        begin = begin + 1
    elif movewhere == "north":
        begin = begin - 1
    elif movewhere == "east":
        begin = begin + 10
    elif movewhere == "west":
        begin = begin - 10
    #print begin, str(int(begin) + hundreds)
    return str(int(begin) + hundreds)

