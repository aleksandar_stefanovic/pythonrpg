class enemy ():
    HP = 0
    Str = 0
    Def = 0
    Acc = 0
    Crit = 0
    ability = ""

    HPmax = 0
    Strnorm = 0
    Defnorm = 0
    Accnorm = 0
    Critnorm = 0

    def printStats (self):
        print 'HP:', self.HP, '/', self.HPmax, '\n', 'Str:', self.Str, '/', self.Strnorm, '\n', 'Def:', self.Def, '/', self.Defnorm, '\n', 'Acc:', self.Acc, '/', self.Accnorm, '\n', 'Crit:', self.Crit, '/', self.Critnorm, '\n', self.ability

class hiv (enemy):
    def __init__ (self):
        self.HP = 10
        self.Str = 6
        self.Def = 1
        self.Acc = 60
        self.ability = "Ability: Can attack opponent's immune system, decreases opponent's defense by 1 each turn"

        self.HPmax = 10
        self.Strnorm = 6
        self.Defnorm = 1
        self.Accnorm = 60

class ecoli (enemy):
    def __init__ (self):
        self.HP = 12
        self.Str = 8
        self.Def = 3
        self.Acc = 80
        self.Crit = 5
        self.ability = 'Ability: Can infect opponent, decreases opponents strength by 1 each turn'

        self.HPmax = 12
        self.Strnorm = 8
        self.Defnorm = 3
        self.Accnorm = 80
        self.Critnorm = 5

class amoeba (enemy):
    def __init__ (self):
        self.HP = 15
        self.Str = 9
        self.Def = 3
        self.Acc = 80
        self.Crit = 10
        self.ability = "Ability 1: partially engulfs opponent during attack, opponent's Def reduces by  1 each turn\nAbility 2: can change shape to avoid attack, opponent's Acc is 3/4 of its normal value"

        self.HPmax = 15
        self.Strnorm = 9
        self.Defnorm = 3
        self.Accnorm = 80
        self.Critnorm = 10

class worm (enemy):
    def __init__ (self):
        self.HP = 15
        self.Str = 5
        self.Def = 3
        self.Acc = 60
        self.Crit = 10
        self.ability = 'Ability: Can grow a new tail if cut in half, when defeated there is a 30% chance of it re-spawning'

        self.HPmax = 15
        self.Strnorm = 5
        self.Defnorm = 3
        self.Accnorm = 60
        self.Critnorm = 10

class ant (enemy):
    def __init__ (self):
        self.HP = 12
        self.Str = 8
        self.Def = 3
        self.Acc = 60
        self.Crit = 10
        self.ability = 'Ability: Can call for backup, Crit increases by 5% each turn (up to 40%)'

        self.HPmax = 12
        self.Strnorm = 8
        self.Defnorm = 3
        self.Accnorm = 60
        self.Critnorm = 10
        
class mantus (enemy):
    def __init__ (self):
        self.HP = 20
        self.Str = 12
        self.Def = 5
        self.Acc = 90
        self.Crit = 10
        self.ability = "Ability 1: seriously intimidating, opponent's Crit is reduced to 0%\nAbility 2: can spare an attack to replenish its energy through prayer, gains back half the damage of the last attack it received every 5 turns"

        self.HPmax = 20
        self.Strnorm = 12
        self.Defnorm = 5
        self.Accnorm = 90
        self.Critnorm = 10

class traitor (enemy):
    def __init__ (self):
        self.HP = 15
        self.Str = 10
        self.Def = 5
        self.Acc = 80
        self.Crit = 10
        self.ability = 'Ability: Can absorb your power, for all damage done to you, the Traitor gains that amount to its health'

        self.HPmax = 15
        self.Strnorm = 10
        self.Defnorm = 5
        self.Accnorm = 80
        self.Critnorm = 10

class theguy (enemy):
    def __init__ (self):
        self.HP = 25
        self.Str = 15
        self.Def = 10
        self.Acc = 90
        self.Crit = 20
        ability = "Ability 1: This guy has a shrink-ray like you, sparing an attack, reduces stats to your base stats (the ones that you started off with) for 2 turns\nAbility 2: This guy is a manipulator to make you let your guard down, opponent's Def is reduced to 3/4 of its normal value"

        self.HPmax = 25
        self.Strnorm = 15
        self.Defnorm = 10
        self.Accnorm = 90
        self.Critnorm = 20


