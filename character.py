class character ():
    HP = 50
    Str = 10
    Def = 5
    Acc = 80
    Crit = 10
    Inv = []
    ability = ''

    HPmax = 50
    Strnorm = 10
    Defnorm = 5
    Accnorm = 80
    Critnorm = 10

    def printStats (self):
        print 'HP:', self.HP, '/', self.HPmax, '\n', 'Str:', self.Str, '/', self.Strnorm, '\n', 'Def:', self.Def, '/', self.Defnorm, '\n', 'Acc:', self.Acc, '/', self.Accnorm, '\n', 'Crit:', self.Crit, '/', self.Critnorm, '\n', 'Inv:', self.Inv, '\n', self.ability
        
    def levelUp (self):
        self.HP += 1
        self.Str += 1
        self.Def += 1

        self.HPmax += 1
        self.Strnorm += 1
        self.Defnorm += 1
        
        if self.Acc < 100:
            self.Acc += 0.5
            self.Accnorm += 1
        if self.Crit < 50:
            self.Crit += 1
            self.Critnorm += 1

    def use (self, x):
        if x in self.Inv:
            self.Inv.remove (x)
            return 1
        else: return 0

class biologist (character):
    def __init__ (self):
        self.Str = 20
        self.Def = 10

        self.Strnorm = 20
        self.Defnorm = 10

        self.ability = 'Ability: immune to all enemy abilities (the biologist knows them all too well)'

class chemist (character):
    def __init__ (self):
        self.Inv = ['potion', 'potion', 'potion', 'water']

        self.ability = 'Ability: starts with chemicals that can be used for attacks'

class physicist (character):
    def __init__ (self):
        self.Acc = 90
        self.Accnorm = 90

        self.ability = "Ability: enemy's Acc is reduced to 3/4 of its original percentage (the physicist can visualize and anticipate the enemy attacks and dodge more easily)"

class compscientist (character):
    def __init__ (self):
        self.Crit = 20
        self.Critnorm = 20

        self.ability = 'Ability: reduces enemy Crit to 0% (the computer scientist likes to keep things easy and predictable)'

class mathematician (character):
    def __init__ (self):
        self.ability = 'Ability: can see all enemy stats'


