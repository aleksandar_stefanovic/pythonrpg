from time import time
import mapsrooms
import random
import character
import enemy
import winsound

def playsound(file): 
    winsound.PlaySound(file,winsound.SND_FILENAME) 
    f = open(file,"r")
    f.close()
    
def combat (enemy, types, etypes, turns):    
    turns += 1
    global player
    global baseON
    global turnsafterbase
    global realstats
    t1 = time()
    player, enemy = ability (player, types, enemy, etypes)
    if types != "5":
        print "Player HP: ", player.HP, " / ", player.HPmax, "\n"
        print etypes, "HP: ", enemy.HP, " / ", enemy.HPmax, "\n"
    choice = raw_input("\nMake your move:\n1. Attack\n2. Inventory\n-->")
    while choice != "1" and choice != "2":
        print "Invalid Choice\n"
        choice = raw_input("Pick a choice:\n1. Attack\n2. Inventory\n-->")
    t2 = time()
    if (t2-t1) > 15:
        print "TOO LONG! MONSTER DODGED ATTACK!"
    else:
        if choice == "1":
            print "You attack the monster!"
            num = random.randrange (10)+1
            if (num*10) <= player.Acc:
                num = random.randrange (10)+1
                if enemy.Def < player.Str:
                    if (num*10) <= player.Crit:
                        enemy.HP -= 3*(player.Str - enemy.Def)
                        print "Critical Hit!\n You did", 3*(player.Str - enemy.Def), "damage on the monster!\n"
                    else:
                       enemy.HP -= (player.Str - enemy.Def)
                       print "You did ", (player.Str - enemy.Def), "damage on the monster!\n"
                else:
                    print "The monsters defense blocked your entire attack!"
                if enemy.HP <= 0:
                    print "The monster is dead!\n"
                    turns = 0
                    if etypes == "worm":
                        num = random.randrange (10)+1
                        if num <= 3:
                            print "A chunk of the worms tail has regrown! Fight the respawned minion!"
                            fight(etypes, types, enemy)
            else:
                print "You missed!\n"    
        elif choice == "2":
            print player.Inv, "\n"
            enemy = useItem (enemy, types, etypes, turns)
    if enemy.HP > 0:
        enemy, player = ability (enemy, etypes, player, types)
        if etypes == "mantus" and  ".0" in str(float(turns)/5):
            print "The mantus absobered your damage!"
            enemy.HP += (player.Str - enemy.Def)
            if enemy.HP > enemy.HPmax:
                enemy.HP = enemy.HPmax
        num = random.randrange(1)
        if etypes == "theguy"  and num == 0 and baseON == 0:
            player = theguyability (player)
            player.printStats()
        else:
            player = theguyabilityCHECK (player)
            print "The monster attacks you!"
            num = random.randrange (10)+1
            if (num*10) <= enemy.Acc:
                num = random.randrange (10)+1
                if player.Def < enemy.Str:
                    if (num*10) <= enemy.Crit:
                        player.HP -= 3*(enemy.Str - player.Def)
                        print "Critical Hit!\nIt did", 3*(enemy.Str - player.Def), "damage on you!\n"
                    else:
                        player.HP -= (enemy.Str - player.Def)
                        print "It did ", (enemy.Str - player.Def), "damage on you!\n"
                else:
                    print "Your defense blocked the entire attack!"
                if player.HP <= 0:
                    print "You died!\n"
                    lose()
            else:
                print "It missed!\n"
    if player.HP > 0 and enemy.HP > 0:
        print "_______________________________________________________________________________________________________________________________________________________________"
        combat (enemy, types, etypes, turns)
    
def useItem (enemy, types, etypes, turns):
    global player
    item = raw_input("Pick an item to use. (type <back> to go back)\n-->")
    while player.use(item) == 0 and item != "back" :
        item = raw_input("Pick an item to use.\n-->")
    if item == "back":
        turns -= 1
        combat (enemy, types, etypes, turns)
    elif item == "potion":
        print "You drank a potion! Gained 5 HP"
        player.HP += 5
        if player.HP > player.HPmax:
            player.HP = player.HPmax
    elif item == "water":
        if etypes == "theguy":
            print "The Guy begins to dissolve from the water! You did it!"
            enemy.HP = 0
        else: print "You drank some water! Refreshing! It did nothing!"
    elif item == "deathstar":
        print "You fire the death stars primary weapon! You enemy is completly obliterated!"
        enemy.HP = 0
    elif item == "shield":
        print "You activate your shield! You are invincible for this fight!"
        player.Def = 100
    return enemy

def ability (caster, types, target, targettypes):
    if types == "2":
        print "Physicist ability activated"
        target.Acc = int(target.Accnorm*0.75)
    if types == "3":
        print "Computer scientist ability activated"
        target.Crit = 0
    if types == "5":
        print "Mathematician ability activated"
        caster.printStats()
        print "\n"
        target.printStats()
    if targettypes != "1":
        if types == "hiv":
            print "HIV virus damages your defense by 1"
            target.Def -= 1
            if target.Def < 0:
                target.Def = 0
        elif types == "ecoli":
            print "E Coli damages your strength by 1"
            target.Str -= 1
            if target.Str < 1:
                target.Str = 1
        elif types == "ant":
            print "Ant increases its critical hit by 5% (40% max)"
            caster.Crit += 5
            if caster.Crit > 40:
                caster.Crit = 40
        elif types == "mantus":
            print "The mantus decreases your critical chance to 0%"
            target.Crit = 0
        elif types == "traitor":
            print "The traitor heals itself!"
            caster.HP += (caster.Str - target.Def)
            if caster.HP > caster.HPmax:
                caster.HP = caster.HPmax
        elif types == "theguy":
            print "The Guy decreases your defense to 3/4 of its original value"
            target.Def = int(target.Defnorm*0.75)
    else:
        print "Biologist immunity blocks the monsters ability"    
    return caster, target
    
def theguyability (player):
    global baseON
    global turnsafterbase
    global realstats
    
    if baseON == 0:
        print "The Guy uses his shrink ray! Reduced to base stats!"
        realstats = [player.HP, player.Str, player.Def, player.Acc, player.Crit]
        player.HP = 50
        if types == "1":
            player.Str = 20
            player.Def = 10
        else:
            player.Str = 10
            player.Def = 5
        if types == "3":
            player.Crit = 20
        else:
            player.Crit =10
        if types == "2":
            player.Acc =90
        else:
            player.Acc =80
        baseON = 1
        turnsafterbase = 0
    return player

def theguyabilityCHECK (player):
    global baseON
    global realstats
    global turnsafterbase
    if baseON == 1:
        turnsafterbase += 1
        if turnsafterbase == 2:
            player.HP = realstats[0]
            player.Str = realstats[1]
            player.Def = realstats[2]
            player.Acc = realstats[3]
            player.Crit = realstats[4]  
            baseON = 0
            print "Shrink ray effect has worn out!"
        
    return player
def fight (etypes, types, enemy):
    global player
    turns = 0
    if etypes == "hiv":
        enemy = enemy.hiv()
    elif etypes == "ecoli":
        enemy = enemy.ecoli()
    elif etypes == "amoeba":
        enemy = enemy.amoeba()
    elif etypes == "worm":
        enemy = enemy.worm()
    elif etypes == "ant":
        enemy = enemy.ant()
    elif etypes == "mantus":
        enemy = enemy.mantus()
    elif etypes == "traitor":
        enemy = enemy.traitor()
    elif etypes == "theguy":
        enemy = enemy.theguy()
    print "\nWild", etypes, "appeared!"
    combat (enemy, types, etypes, turns)
    player.Str = player.Strnorm
    player.Def = player.Defnorm
    player.Acc = player.Accnorm
    player.Crit = player.Critnorm
    player.levelUp()
    print "Level Up!"

def roomconditions(room, LevelOneRooms):
    global player
    num = 0
    if room == int(str(LevelOneRooms[0:1]).replace("[","").replace("]","")): player.HP = player.HPmax
    elif room == int(str(LevelOneRooms[4:5]).replace("[","").replace("]","")): player.HP = player.HP/2
    elif room == int(str(LevelOneRooms[7:8]).replace("[","").replace("]","")): player.Acc = int(player.Acc*0.9)
    elif room == int(str(LevelOneRooms[6:7]).replace("[","").replace("]","")): player.Inv.append("potion")
    elif room == int(str(LevelOneRooms[8:9]).replace("[","").replace("]","")):
        choice = raw_input ("-->")
        while  "frostbite" not in choice and "safe" not in choice:
              choice = raw_input ("-->")
        if "frostbite" in choice:
            print "You barely make it through. Frostbite makes you lose a quarter of your health. You gain a bottle of water."
            player.HP = int(player.HP*0.75)
            player.Inv.append("water")
        else: print "You saftly make it through."
    elif room == 213: player.HP = player.HP + player.HP*0.2
    elif room == 221: num = 1
    elif room == 323: player.Inv.append("deathstar")
    elif room == 331: player.Crit = 0
    elif room == 332: player.HP = player.HP/2
    elif room == 333: player.Crit = 100
    elif room == 342: player.Inv.append("shield")
    n = random.randrange (10)+1
    if n <= 3:
        questions = ["Organisms struggle to survive in a habitat-what is this called?","Where an organism lives and receives what it needs to live is called its?","EM waves with the highest frequencies are?","The material a wave travels through is: ","Day and night are caused by:","Herbivores eat:","Organisms that make their own food:","A relationship between 2 species that benefits at least one of the species is","One revolution of the Earth takes","The distance from the rest position to the highest point of a wave is"]
        print "TRAP! Answer a science based question!"
        pick = random.randrange(10)
        answer = raw_input(questions[pick] + "\n-->")
        if pick == 0:
            if answer == "competition":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 1:
            if answer == "habitat":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 2:
            if answer == "gamma rays":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 3:
            if answer == "medium":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 4:
            if answer == "earths rotation":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 5:
            if answer == "plants":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 6:
            if answer == "producer":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 7:
            if answer == "symbiosis":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 8:
            if answer == "one year":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0" 
        elif pick == 9:
            if answer == "amplitude":
                print "Correct!"
                answer = "1"
            else:
                print "Wrong!"
                answer = "0"
        if answer == "0":
            cond = random.randrange (6)+1
            if cond == 1:
                player.HP = player.HP/2
                print "Lose half your HP!"
            elif cond == 2:
                player.HP = int(player.HP*0.75)
                print "Lose a quarter of your HP!"
            elif cond == 3:
                print "Nothing happened! Your safe!"
            elif cond == 4:
                player.Acc = player.Acc/2
                print "Lose half your accuracy!"
            elif cond == 5:
                player.Crit = player.Crit/2
                print "Lose half your critical hit chance!"
            elif cond == 5:
                player.Def = player.Def/2
                print "Lose half your defense!"
            elif cond == 6:
                player.Str = player.Str/2
                print "Lose half your strength!"
        
    return num
        


def win():
    print """
_____.___.               __      __.__      ._.
\__  |   | ____  __ __  /  \    /  \__| ____| |
 /   |   |/  _ \|  |  \ \   \/\/   /  |/    \ |
 \____   (  <_> )  |  /  \        /|  |   |  \|
 / ______|\____/|____/    \__/\  / |__|___|  /_
 \/                            \/          \/\/
"""
    playsound("kidscheering.wav")
    
def lose():
    print """
 ________                        ________                     
 /  _____/_____    _____   ____   \_____  \___  __ ___________ 
/   \  ___\__  \  /     \_/ __ \   /   |   \  \/ // __ \_  __ \
\    \_\  \/ __ \|  Y Y  \  ___/  /    |    \   /\  ___/|  | \/
 \______  (____  /__|_|  /\___  > \_______  /\_/  \___  >__|   
        \/     \/      \/     \/          \/          \/       

"""
    playsound("booing.wav")
    playsound("BOO_YOU_STINK.wav")
print "*Turn speakers on*"    
playsound ("ShrinkLazer.wav")
playsound ("ShrinkSound.wav")
playsound ("Fall.wav")
print "Yall jus got shrunk!\n You work in a lab that studies the cubesquare law and your team successfully produces a sizemanipulating device. Some of your teammates want to use the technology to take over the world but you refuse. So they betray you and shrink you down to microscopic size to eliminate your threat to their plan. You kept your own device but you do not have enough power to grow yourself back. You need to travel through microscopic followed by nugsized environments to get to a power source which is defended by a boss and grow yourself. The third level is the one where you take down your exlabmates the traitors."
playsound ("Background.wav")
playsound ("Background.wav")
raw_input("WELCOME! TO THE GREATEST GAMING EXPERIENCE EVER! WELCOME TO <press enter>")
print  """
 _________.__                        __    
 /   _____/|  |_________ __ __  ____ |  | __
 \_____  \ |  |  \_  __ \  |  \/    \|  |/ /
 /        \|   Y  \  | \/  |  /   |  \    < 
/_______  /|___|  /__|  |____/|___|  /__|_ \
        \/      \/                 \/     \/
       """
raw_input("Press enter to begin...")
print "Generating world, please wait... (This may take a few minutes)"
playsound ("MYSTERIOUS_SOUND_FILE.wav")
print "Here we go!"
#MAIN PROGRAM
choice = raw_input ("Pick a class.\n1. Biologist \n2. Physicist \n3. Computer Scientist \n4. Chemist \n5. Mathematician\n-->")
while choice != "1" and choice != "2" and choice != "3" and choice != "4" and choice != "5":
    print "Invalid Choice\n"
    choice = raw_input ("Pick a class.\n1. Biologist \n2. Physicist \n3. Computer Scientist \n4. Chemist \n5. Mathematician\n-->")    
if choice == "1":
    player = character.biologist()
elif choice == "2":
    player = character.physicist()
elif choice == "3":
    player = character.compscientist()
elif choice == "4":
    player = character.chemist()
elif choice == "5":
    player = character.mathematician()

realstats = []
baseON = 0
turnsafterbase = 0
types = choice
LevelOneRooms = []


counter = 1
while counter == 1:
    LevelOneRooms = [112,113,121,122,123,131,132,133,141,142] #Room locations, rooms 11,42 are permanaent
    random.shuffle(LevelOneRooms)
    place = 111
    while place != 143:
        etypes, many = mapsrooms.descriptions(place, LevelOneRooms)
        num = roomconditions(place, LevelOneRooms)
        for i in range (0, many, 1):
            fight(etypes, types, enemy)
        print "\n_______________________________________________________________________________________________________________________________________________________________"
        newroom = int(mapsrooms.movement(place))
        place = newroom
    etypes, many = mapsrooms.descriptions(143, LevelOneRooms)
    fight(etypes, types, enemy)
    print "\nNEXT LEVEL\n=========="
    counter = counter + 1
while counter == 2:
    place = 211
    while place != 243:
        etypes, many = mapsrooms.descriptions(place, LevelOneRooms)
        num = roomconditions(place, LevelOneRooms)
        if num == 1: t1 = time()
        for i in range (0, many, 1):
            fight(etypes, types, enemy)
        print "\n_______________________________________________________________________________________________________________________________________________________________"
        newroom = int(mapsrooms.movement(place))
        if num == 1:
            t2 = time()
            player.HP -= int(t2 - t1)
        place = newroom
    etypes, many = mapsrooms.descriptions(243, LevelOneRooms)
    fight(etypes, types, enemy)
    print "\nNEXT LEVEL\n=========="
    counter = counter + 1
while counter == 3:
    place = 311
    while place != 343:
        etypes, many = mapsrooms.descriptions(place, LevelOneRooms)
        num = roomconditions(place, LevelOneRooms)
        for i in range (0, many, 1):
            fight(etypes, types, enemy)
        print "\n_______________________________________________________________________________________________________________________________________________________________"
        newroom = int(mapsrooms.movement(place))
        place = newroom
    etypes, many = mapsrooms.descriptions(343, LevelOneRooms)
    fight(etypes, types, enemy)
    win()
    counter = counter + 1




